import { Controller, Get } from '@nestjs/common';
import { Link } from './link.entity';
import { LinksService } from './links.service';

@Controller('links')
export class LinksController {
  constructor(private readonly linksService: LinksService) {}

  @Get()
  async getAllLinks(): Promise<Array<Link>> {
    return this.linksService.getAllLinks();
  }
}
