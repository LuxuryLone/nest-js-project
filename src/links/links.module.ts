import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LinkRepository } from './links.repository';
import { LinksService } from './links.service';
import { LinksController } from './links.controller';

@Module({
  imports: [TypeOrmModule.forFeature([LinkRepository])],
  providers: [LinksService],
  controllers: [LinksController],
})
export class LinksModule {}
