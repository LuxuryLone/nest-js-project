import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Link } from './link.entity';
import { LinkRepository } from './links.repository';

@Injectable()
export class LinksService {
  constructor(
    @InjectRepository(LinkRepository)
    private readonly linksRepository: LinkRepository,
  ) {}

  async getAllLinks(): Promise<Array<Link>> {
    return this.linksRepository.find({});
  }
}
